package rp

import (
	"context"
	"errors"
	"github.com/gookit/validate"
	"github.com/sirupsen/logrus"
	"grpc_test/pkg/rusprofile"
)

type impl struct {
	log        *logrus.Logger
	rusprofile rusprofile.Rusprofile
}

func (i impl) GetInfoByInn(ctx context.Context, request *GetInfoByInnRequest) (*GetInfoByInnResponse, error) {
	q := rusprofile.RequestQuery{Inn: request.Inn}

	v := validate.Struct(q)
	if !v.Validate() {
		errs := v.Errors
		i.log.Errorf("Validation errors is: %v", errs)
		return nil, errs.ErrOrNil()
	}

	firmInfo, err := i.rusprofile.GetInfoByInn(q)
	if err != nil {
		return nil, err
	}
	var firm Firm
	if firmInfo != nil {
		firm = Firm{
			Inn:     firmInfo.Inn,
			Kpp:     firmInfo.Kpp,
			Name:    firmInfo.Name,
			CeoName: firmInfo.CeoName,
			Url:     firmInfo.Url,
		}
		return &GetInfoByInnResponse{
			Firm: &firm,
		}, nil
	}
	return nil, errors.New("unknown error")
}

func (i impl) mustEmbedUnimplementedRusprofileServer() {
	//TODO implement me
	panic("implement me")
}

func GetServer(log *logrus.Logger, rusprofile rusprofile.Rusprofile) RusprofileServer {
	return impl{log: log, rusprofile: rusprofile}
}
