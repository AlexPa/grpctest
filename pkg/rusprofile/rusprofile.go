package rusprofile

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/sirupsen/logrus"
	"io"
	"math/rand"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"time"
)

const rusprofileScheme string = "https"
const rusprofileHost string = "www.rusprofile.ru"
const delayTime time.Duration = 5

type impl struct {
	log *logrus.Logger
}
type RequestQuery struct {
	Inn string `validate:"required:required|IsInnValidator" message:"wrong inn format" label:"ИНН"`
}

type FirmInfo struct {
	Inn     string `json:"inn"`
	Kpp     string
	Name    string `json:"name"`
	CeoName string `json:"ceo_name"`
	Url     string `json:"url"`
}

type searchResponse struct {
	Code    int        `json:"code"`
	IpCount int        `json:"ip_count"`
	Message string     `json:"message"`
	Success bool       `json:"success"`
	Ul      []FirmInfo `json:"ul"`
	Ip      []FirmInfo `json:"ip"`
	UlCount int        `json:"ul_count"`
}

func GetImpl(log *logrus.Logger) Rusprofile {
	return impl{log: log}
}
func (q RequestQuery) IsInnValidator(value interface{}) bool {
	str := value.(string)
	l := len(str)
	if l < 10 || l > 12 {
		return false
	}
	valid := regexp.MustCompile(`^\d+$`).MatchString(str)
	if !valid {
		return false
	}
	//TODO: implement checkout that first two digits is code of Russia region
	//first2 := getFirstNSymbols(str, 2)

	return true
}
func (i impl) getKppByUrl(url string) string {
	//TODO: grab kpp from url
	return "1000000000 (Not implemented)"
}
func (i impl) fillHeaders(req *http.Request) {
	//curl 'https://www.rusprofile.ru/' \
	//-H 'authority: www.rusprofile.ru' \
	//-H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
	//-H 'accept-language: en-US,en;q=0.9,ru;q=0.8,it;q=0.7' \
	//-H 'cache-control: max-age=0' \
	//-H 'cookie: fbb_s=1; exp-rpf-6850=a; fbb_u=1681614217' \
	//-H 'sec-ch-ua: "Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"' \
	//-H 'sec-ch-ua-mobile: ?0' \
	//-H 'sec-ch-ua-platform: "Linux"' \
	//-H 'sec-fetch-dest: document' \
	//-H 'sec-fetch-mode: navigate' \
	//-H 'sec-fetch-site: none' \
	//-H 'sec-fetch-user: ?1' \
	//-H 'upgrade-insecure-requests: 1' \
	//-H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36' \
	//--compressed
	req.Header.Add("authority", "www.rusprofile.ru")
	req.Header.Add("accept-language", "en-US,en;q=0.9,ru;q=0.8,it;q=0.7")
	req.Header.Add("cache-control", "max-age=0")
	req.Header.Add("cookie", "fbb_s=1; exp-rpf-6850=a; fbb_u=1681614217")
	//req.Header.Add("cookie", "fbb_s=1; exp-rpf-6850=a; fbb_u="+strconv.FormatInt(time.Now().Unix(), 10))
	req.Header.Add("sec-ch-ua", "\"Not?A_Brand\";v=\"8\", \"Chromium\";v=\"108\", \"Google Chrome\";v=\"108\"")
	req.Header.Add("sec-ch-ua-mobile", "?0")
	req.Header.Add("sec-ch-ua-platform", "\"Linux\"")
	req.Header.Add("sec-fetch-dest", "document")
	req.Header.Add("sec-fetch-mode", "navigate")
	req.Header.Add("sec-fetch-site", "none")
	req.Header.Add("sec-fetch-user", "?1")
	req.Header.Add("upgrade-insecure-requests", "1")
	req.Header.Add("accept", "accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36")
}
func (i impl) GetInfoByInn(q RequestQuery) (*FirmInfo, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*delayTime)
	defer cancel()

	values := url.Values{}
	values.Add("query", q.Inn)
	values.Add("action", "search")
	//values.Add("cacheKey", "0.6012577435902937")
	values.Add("cacheKey", "0.6012577435902"+strconv.Itoa(rand.Intn(999-100)+100))
	i.log.Debugf("values is: %v", values)

	u := url.URL{
		Scheme:   rusprofileScheme,
		Host:     rusprofileHost,
		Path:     "ajax.php",
		RawQuery: values.Encode(),
	}

	i.log.Debugf("Url is: %s", u.String())

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, u.String(), nil)
	if err != nil {
		i.log.Errorf("Url request error: %v", err)
		return nil, err
	}
	i.fillHeaders(req)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		i.log.Errorf("Url response error: %v", err)
		return nil, err
	}

	var searchResponse searchResponse
	bytesArray, err := io.ReadAll(resp.Body)
	if err != nil {
		i.log.Errorf("Read bytes error: %v", err)
		return nil, err
	}
	str := string(bytesArray)
	i.log.Debugf("FirmInfoResponse is: %s", str)
	err = json.Unmarshal([]byte(str), &searchResponse)
	if err != nil {
		i.log.Errorf("Unmarshal error: %v", err)
		return nil, err
	}
	i.log.Debugf("FirmInfo: %v", searchResponse)
	i.log.Debugf("FirmInfo success: %v", searchResponse.Success)
	if searchResponse.Success == true {
		// Это про компания
		if len(searchResponse.Ul) >= 1 {
			ret := &searchResponse.Ul[0]
			ret.Kpp = i.getKppByUrl(ret.Url)
			return ret, nil
		} else if len(searchResponse.Ip) >= 1 {
			ret := &searchResponse.Ip[0]
			return ret, nil
		}
	} else {
		return nil, errors.New(searchResponse.Message)
	}
	return nil, errors.New("firm not found")
}

type Rusprofile interface {
	GetInfoByInn(q RequestQuery) (*FirmInfo, error)
}

func getFirstNSymbols(s string, n int) string {
	i := 0
	for j := range s {
		if i == n {
			return s[:j]
		}
		i++
	}
	return s
}
