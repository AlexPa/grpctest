package limiter

import (
	"github.com/grpc-ecosystem/go-grpc-middleware/ratelimit"
	"github.com/sirupsen/logrus"
	"strconv"
	"time"
)

var lastTime int64

type myLimiter struct {
	seconds float64
	log     *logrus.Logger
}

func (m myLimiter) Limit() (ret bool) {
	newValue := time.Now().UnixMicro()
	defer func(newValue int64) {
		if ret == false {
			lastTime = newValue
		}
	}(newValue)

	if lastTime == 0 {
		return false
	}
	dur, _ := time.ParseDuration(strconv.FormatInt(newValue-lastTime, 10) + "µs")
	m.log.Debugf("Duration seconds: %f", dur.Seconds())
	if dur.Seconds() < m.seconds {
		return true
	}

	return false
}

func NewLimiter(seconds float64, log *logrus.Logger) ratelimit.Limiter {
	return myLimiter{
		seconds: seconds,
		log:     log,
	}
}
