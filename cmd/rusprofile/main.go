package main

import (
	"context"
	"github.com/grpc-ecosystem/go-grpc-middleware/ratelimit"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"grpc_test/api/proto/rp"
	"grpc_test/cmd/limiter"
	"grpc_test/logging"
	"grpc_test/pkg/rusprofile"
	"net"
	"net/http"
)

var log *logrus.Logger

const (
	portREST    string = "8084"
	portGRPC    string = "8085"
	portSWAGGER string = "8086"
	swaggerURL  string = "/swaggerui/"
)

func main() {
	log = logging.GetLogger()
	go runRest()
	go runSwagger()
	runGrpc()
}

/*
Т.к. в grpc-gateway нельзя в Handle ставить trailing slash, то непосредственно в GO
сделать КРАСИВО документацию API по адресу /swaggerui не получается. Поэтому будет через nginx
*/
func runSwagger() {
	handler := http.StripPrefix(swaggerURL, http.FileServer(http.Dir("./web/swagger/rp")))
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Debugf("URL: %s", r.URL.Path)
		if r.URL.Path != swaggerURL {
			http.Redirect(w, r, swaggerURL, http.StatusFound)
		}
	})
	http.Handle(swaggerURL, handler)

	log.Infof("SWAGGER server listening at %s", portSWAGGER)
	if err := http.ListenAndServe("0.0.0.0:"+portSWAGGER, nil); err != nil {
		panic(err)
	}
}
func runGrpc() {
	ruspr := rusprofile.GetImpl(log)

	// For handle captcha appearing purpose
	lim := limiter.NewLimiter(3.1, log)
	s := grpc.NewServer(grpc.ChainUnaryInterceptor(
		ratelimit.UnaryServerInterceptor(lim),
	))
	rpServer := rp.GetServer(log, ruspr)
	rp.RegisterRusprofileServer(s, rpServer)

	listen, err := net.Listen("tcp", "0.0.0.0:"+portGRPC)
	if err != nil {
		panic(err)
	}

	log.Infof("GRPC server listening at %s", portGRPC)
	if err := s.Serve(listen); err != nil {
		panic(err)
	}
}
func runRest() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()

	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}

	err := rp.RegisterRusprofileHandlerFromEndpoint(ctx, mux, "0.0.0.0:"+portGRPC, opts)

	if err != nil {
		panic(err)
	}

	srv := &http.Server{
		Addr:    ":" + portREST,
		Handler: cors(mux),
	}
	log.Infof("REST server listening at %s", portREST)
	if err := srv.ListenAndServe(); err != nil {
		panic(err)
	}
}

func cors(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization, ResponseType")
		if r.Method == "OPTIONS" {
			return
		}
		h.ServeHTTP(w, r)
	})
}
